all:
	g++ hog.cpp -I../opencv-3.1.0/release/include -L../opencv-3.1.0/release/lib -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_calib3d -lopencv_features2d -lopencv_flann  -lopencv_ml -lopencv_objdetect -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_video -lopencv_videostab -lopencv_videoio -lopencv_imgcodecs -o hog
